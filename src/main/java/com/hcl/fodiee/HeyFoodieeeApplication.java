package com.hcl.fodiee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeyFoodieeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeyFoodieeeApplication.class, args);
	}

}
