package com.hcl.fodiee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.serviceImpl.UserServiceImpl;
import com.hcl.fodiee.serviceImpl.VendorServiceImpl;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("")
public class LoginController {

   private final UserServiceImpl userServiceImpl;
   private final VendorServiceImpl vendorServiceImpl;

   @PostMapping("/login")
   public ResponseEntity<ResponseDto> login(@RequestParam String Id, @RequestParam String password) {
       if (Id.startsWith("CID")) {
           return new ResponseEntity<>(userServiceImpl.login(Id, password), HttpStatus.OK);
       } else if (Id.startsWith("VID")) {
           return new ResponseEntity<>(vendorServiceImpl.login(Id, password), HttpStatus.OK);
       } else {
           return new ResponseEntity<>(ResponseDto.builder().httpCode(401).message("Invalid credentials").build(), HttpStatus.UNAUTHORIZED);
       }
   }
//   @PostMapping("/logout")
//   public ResponseEntity<ResponseDto> logout(@RequestParam String Id) {
//       if (Id.startsWith("CID")) {
//    	   userServiceImpl.logout(Id);
//           return ResponseEntity.ok(ResponseDto.builder().httpCode(200).message("User Logout successful").build());
//       } else if (Id.startsWith("VID")) {
//    	   vendorServiceImpl.logout(Id);
//           return ResponseEntity.ok(ResponseDto.builder().httpCode(200).message("Vendor Logout successful").build());
//       } else {
//           return ResponseEntity.badRequest().body(ResponseDto.builder().httpCode(400).message("Id Not Found").build());
//       }
//   }
}