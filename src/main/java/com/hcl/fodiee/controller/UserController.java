package com.hcl.fodiee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.UserDto;
import com.hcl.fodiee.serviceImpl.UserServiceImpl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {
	private final UserServiceImpl userServiceImpl;
	
	@PostMapping("/")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody UserDto userDto){
		return new ResponseEntity<ResponseDto>(userServiceImpl.register(userDto),HttpStatus.CREATED);
	}
}
