package com.hcl.fodiee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.VendorDto;
import com.hcl.fodiee.serviceImpl.VendorServiceImpl;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/vendor")
public class VendorController {
	private final VendorServiceImpl vendorServiceImpl;

	@PostMapping("/")
	public ResponseEntity<ResponseDto> register(@Valid @RequestBody VendorDto vendorDto) {
		return new ResponseEntity<ResponseDto>(vendorServiceImpl.register(vendorDto), HttpStatus.CREATED);
	}
}
