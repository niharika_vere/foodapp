package com.hcl.fodiee.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto {
	@NotBlank(message = "name is requiredfield")
	private String Id;
	@NotBlank(message = "name is requiredfield")
	private String password;

}
