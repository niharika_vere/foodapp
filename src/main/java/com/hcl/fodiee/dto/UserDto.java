package com.hcl.fodiee.dto;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
	@NotBlank(message = "name is requiredfield")
	private String name;
	
	@Email(message = "invalid email")
	@NotBlank
	private String email;
	
	@NotBlank(message = "mobileNumber is required")
	@Size(min = 10, max = 10,message="number should be 10 numbers only")
	private String contactNo;

}

