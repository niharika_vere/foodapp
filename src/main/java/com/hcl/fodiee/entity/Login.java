
package com.hcl.fodiee.entity;
 
import java.time.LocalDateTime;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Login {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long loginId;
 
    private String Id;
    private String password;
 
    @CreationTimestamp
    private LocalDateTime loginTime;
 
    @UpdateTimestamp
    private LocalDateTime logoutTime;
 
    
    private boolean status;  
    @ManyToOne
    private User user;
    @ManyToOne
    private Vendor vendor;
}