package com.hcl.fodiee.entity;
import java.util.Set;

import com.hcl.fodiee.dto.Roles;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@Data
@ToString
@NoArgsConstructor
@Entity
public class Vendor {
	@Id
	private String vendorId;
	private String vendorname;
	private String email;
	private String password;
	private String contactNo;
	
	@Enumerated(EnumType.STRING)
	private Roles role;
	@OneToMany(mappedBy = "vendor",cascade = CascadeType.ALL)
	private Set<Login>login;
}

