package com.hcl.fodiee.exceptions;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleinvaliddata(MethodArgumentNotValidException ex) {
		Map<String, String> errMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors().forEach(err -> {
			errMap.put(err.getField(), err.getDefaultMessage());
		});
		return errMap;
	}
	@ExceptionHandler(UserAlreadyExists.class)
	public ResponseEntity<String> handleUserAlreadyExists(UserAlreadyExists exception) {
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(exception.getMessage());

	}
}
