package com.hcl.fodiee.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.fodiee.entity.Login;


public interface LoginRepository extends JpaRepository<Login, Long>{
	//@Query("SELECT 1 FROM LOGIN WHERE 1.id=:id AND 1.status=:status")
	//Optional<Login>findByUser_IdAndStatus(String customerId,boolean status);

	Optional<Login> findByUser_IdAndStatus(String customerId, boolean status);

	Optional<Login> findByVendor_IdAndStatus(String vendorId, boolean status);

}
