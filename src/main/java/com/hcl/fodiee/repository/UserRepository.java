package com.hcl.fodiee.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.fodiee.entity.User;


public interface UserRepository extends JpaRepository<User, String> {

	Optional<User> findByEmail(String email);
}
