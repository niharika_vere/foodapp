package com.hcl.fodiee.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.fodiee.entity.Vendor;


public interface VendorRepository extends JpaRepository<Vendor, String> {

	Optional<Vendor> findByEmail(String email);




}
