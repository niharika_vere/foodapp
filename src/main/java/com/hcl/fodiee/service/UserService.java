package com.hcl.fodiee.service;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.UserDto;

import jakarta.validation.Valid;

public interface UserService {
	ResponseDto register(@Valid UserDto userDto) ;
	ResponseDto login(String customerId,String password);
	//ResponseDto logout(String customerId);
}
