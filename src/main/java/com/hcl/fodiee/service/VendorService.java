package com.hcl.fodiee.service;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.VendorDto;

import jakarta.validation.Valid;

public interface VendorService {
	ResponseDto register(@Valid VendorDto vendorDto);

	ResponseDto login(String vendorId,String password);
	//ResponseDto logout(String vendorId);

}
