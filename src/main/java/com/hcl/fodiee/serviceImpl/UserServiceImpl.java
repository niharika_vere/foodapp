package com.hcl.fodiee.serviceImpl;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.Roles;
import com.hcl.fodiee.dto.UserDto;
import com.hcl.fodiee.entity.Login;
import com.hcl.fodiee.entity.User;
import com.hcl.fodiee.exceptions.UserAlreadyExists;
import com.hcl.fodiee.repository.LoginRepository;
import com.hcl.fodiee.repository.UserRepository;
import com.hcl.fodiee.service.UserService;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService{
	private final UserRepository userRepository;
	private final LoginRepository loginRepository;

	public ResponseDto register(@Valid UserDto userDto) {
		Optional<User> user1=userRepository.findByEmail(userDto.getEmail());
		if(user1.isPresent()) {
			throw new UserAlreadyExists("User already exists");
		}
		User user=new User();
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2="CID"+id;
		user.setCustomerId(id2);
		user.setName(userDto.getName());
		user.setEmail(userDto.getEmail());
		user.setContactNo(userDto.getContactNo());
		String characters2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String pwd = RandomStringUtils.random(8, characters2);
		user.setPassword(pwd);
		user.setRole(Roles.USER);
		userRepository.save(user);
		return ResponseDto.builder().httpCode(201).message("Registered successfully").build();
	}
	public ResponseDto login(String customerId,String password) {
        Optional<User> user = userRepository.findById(customerId);
 
        if (user.isPresent()&& user.get().getPassword().equals(password)) {
        	Optional<Login> existingLogin = loginRepository.findByUser_IdAndStatus(customerId, true);
       	 
	        if (existingLogin.isPresent()) {
	            return ResponseDto.builder().httpCode(400).message("User is already logged in").build();
	        }

            Login loginDetails = new Login();
            loginDetails.setId(user.get().getCustomerId());
            loginDetails.setPassword(user.get().getPassword());
            loginDetails.setUser(user.get());
            
            loginDetails.setStatus(true);
            loginRepository.save(loginDetails);
 	 
            return ResponseDto.builder().httpCode(200).message("User Login successful").build();
        } else {
            return ResponseDto.builder().httpCode(404).message("User not found").build();
        }
    }
//	  public ResponseDto logout(String customerId) {
//	        Optional<User> user = userRepository.findById(customerId);
//	 
//	        if (user.isPresent()) {	 
//	        	   Optional<Login> existingLogin = loginRepository.findByIdAndStatus(customerId, true);
//	  			 
//			        if (existingLogin.isPresent()) {
//			        	Login loginDetails = new Login();
//			            loginDetails.setId(user.get().getCustomerId());
//			            loginDetails.setStatus(false);
//			            
//			            loginRepository.save(loginDetails);
//			 	 
//			            return ResponseDto.builder().httpCode(200).message("Vendor Logout successful").build();
//			        } else {
//			            return ResponseDto.builder().httpCode(400).message("Vendor is not logged in").build();
//			        }
//			    } else {
//			        return ResponseDto.builder().httpCode(404).message("Vendor not found").build();
//			    }
//			    }
//

}
