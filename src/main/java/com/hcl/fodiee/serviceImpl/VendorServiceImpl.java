package com.hcl.fodiee.serviceImpl;

import java.util.Optional;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.hcl.fodiee.dto.ResponseDto;
import com.hcl.fodiee.dto.Roles;
import com.hcl.fodiee.dto.VendorDto;
import com.hcl.fodiee.entity.Login;
import com.hcl.fodiee.entity.Vendor;
import com.hcl.fodiee.exceptions.UserAlreadyExists;
import com.hcl.fodiee.repository.LoginRepository;
import com.hcl.fodiee.repository.VendorRepository;
import com.hcl.fodiee.service.VendorService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
@RequiredArgsConstructor
@Service
public class VendorServiceImpl implements VendorService {
	private final VendorRepository vendorRepository;
	private final LoginRepository loginRepository;

	public ResponseDto register(@Valid VendorDto vendorDto) {
		Optional<Vendor> vendor = vendorRepository.findByEmail(vendorDto.getEmail());
		if (vendor.isPresent()) {
			throw new UserAlreadyExists("User already exists");
		}
		Vendor register = new Vendor();
		register.setVendorname(vendorDto.getVendorname());
		register.setEmail(vendorDto.getEmail());
		register.setContactNo(vendorDto.getContactNo());
		register.setRole(Roles.VENDOR);
		String characters = "0123456789";
		String id = RandomStringUtils.random(6, characters);
		String id2 = "VID" + id;
		register.setVendorId(id2);
		String characters2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		String pwd = RandomStringUtils.random(8, characters2);
		register.setPassword(pwd);
		vendorRepository.save(register);
		return ResponseDto.builder().httpCode(201).message("Registered successfully").build();

	}
	public ResponseDto login(String vendorId,String password) {
        Optional<Vendor> vendor = vendorRepository.findById(vendorId);
 
        if (vendor.isPresent()&& vendor.get().getPassword().equals(password)) {
        	Optional<Login> existingLogin = loginRepository.findByVendor_IdAndStatus(vendorId, true);
        	 
        	        if (existingLogin.isPresent()) {
        	            return ResponseDto.builder().httpCode(400).message("Vendor is already logged in").build();
        	        }
 
            Login loginDetails = new Login();
            loginDetails.setId(vendor.get().getVendorId());
            loginDetails.setPassword(vendor.get().getPassword());
            loginDetails.setVendor(vendor.get());
            loginDetails.setStatus(true);;
            loginRepository.save(loginDetails);
 	 
            return ResponseDto.builder().httpCode(200).message("Vendor Login successful").build();
        } else {
            return ResponseDto.builder().httpCode(404).message("Vendor not found").build();
        }
        
    }
//	 public ResponseDto logout(String vendorId) {
//			    Optional<Vendor> vendor = vendorRepository.findById(vendorId);
//			 
//			    if (vendor.isPresent()) {
//			        Optional<Login> existingLogin = loginRepository.findByIdAndStatus(vendorId, true);
//			 
//			        if (existingLogin.isPresent()) {
//			        	Login loginDetails = new Login();
//			            loginDetails.setId(vendor.get().getVendorId());
//			            loginDetails.setStatus(false);
//			            
//			            loginRepository.save(loginDetails);
//			 	 
//			            return ResponseDto.builder().httpCode(200).message("Vendor Logout successful").build();
//			        } else {
//			            return ResponseDto.builder().httpCode(400).message("Vendor is not logged in").build();
//			        }
//			    } else {
//			        return ResponseDto.builder().httpCode(404).message("Vendor not found").build();
//			    }
//			    
//			    
//			    }
	
}
